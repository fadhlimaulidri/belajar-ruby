## Install RVM & Ruby

1. Download installer

   https://rubyinstaller.org/downloads/

   Ruby+Devkit 2.6.X (x64)

2. Check ruby
   ```sh
   ruby -v

   => ruby 2.6.5p114 (2019-10-01 revision 67812) [x86_64-darwin18]
   ```

