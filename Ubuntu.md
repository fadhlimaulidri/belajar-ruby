## Install RVM & Ruby

1. RVM

    ```sh
    curl -L get.rvm.io | bash -s stable
    source ~/.bash_profile
    ```

    Then run this command and follow the instructions

    ```sh
    rvm requirements
    ```

2. Ruby 2.6.5

    ```sh
    rvm install 2.6.5
    ```

3. Check ruby
   ```sh
   ruby -v

   => ruby 2.6.5p114 (2019-10-01 revision 67812) [x86_64-darwin18]
   ```

